<!DOCTYPE html>
<html>
<head>
    <title>Números Primos</title>
    <style>
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        h1 {
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="number"] {
            padding: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        button {
            background-color: #008CBA;
            color: #fff;
            padding: 10px 20px;
            border-radius: 5px;
            border: none;
            cursor: pointer;
        }

        button:hover {
            background-color: #006D9C;
        }

    </style>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="container">
        <h1>Números Primos</h1>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label for="inicio">Ingrese el número de inicio:</label>
            <input type="number" id="inicio" name="inicio" required>
            <label for="fin">Ingrese el número de fin:</label>
            <input type="number" id="fin" name="fin" required>
            <button type="submit">Calcular</button>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            // Función para verificar si un número es primo
            function esPrimo($numero) {
                if ($numero <= 1) {
                    return false;
                }
                for ($i = 2; $i <= sqrt($numero); $i++) {
                    if ($numero % $i == 0) {
                        return false;
                    }
                }
                return true;
            }

            // Contador de números primos
            $contadorPrimos = 0;

            // Verificar cada número en el rango
            for ($num = $inicio; $num <= $fin; $num++) {
                if (esPrimo($num)) {
                    $contadorPrimos++;
                }
            }

            echo "<h2>Resultado</h2>";
            echo "<p>Hay $contadorPrimos números primos en el rango del $inicio al $fin.</p>";
        }
        ?>
    </div>
</body>
</html>
