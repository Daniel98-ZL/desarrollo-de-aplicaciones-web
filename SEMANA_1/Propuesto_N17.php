<!DOCTYPE html>
<html>
<head>
    <title>Calcular Saldo Actual</title>
    <style>
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        h1 {
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="number"] {
            padding: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        select {
            padding: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        button {
            background-color: #008CBA;
            color: #fff;
            padding: 10px 20px;
            border-radius: 5px;
            border: none;
            cursor: pointer;
        }

        button:hover {
            background-color: #006D9C;
        }

    </style>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="container">
        <h1>Calcular Saldo Actual</h1>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label for="saldo_anterior">Saldo anterior:</label>
            <input type="number" id="saldo_anterior" name="saldo_anterior" required>
            <label for="tipo_movimiento">Tipo de movimiento:</label>
            <select id="tipo_movimiento" name="tipo_movimiento" required>
                <option value="R">Retiro</option>
                <option value="D">Depósito</option>
            </select>
            <label for="monto">Monto de la transacción:</label>
            <input type="number" id="monto" name="monto" required>
            <button type="submit">Calcular</button>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $saldo_anterior = $_POST['saldo_anterior'];
            $tipo_movimiento = $_POST['tipo_movimiento'];
            $monto = $_POST['monto'];

            // Calcular el saldo actual
            if ($tipo_movimiento == "R") {
                $saldo_actual = $saldo_anterior - $monto;
            } elseif ($tipo_movimiento == "D") {
                $saldo_actual = $saldo_anterior + $monto;
            } else {
                $saldo_actual = $saldo_anterior;
            }

            echo "<h2>Resultado</h2>";
            echo "<p>Saldo actual: $saldo_actual</p>";
        }
        ?>
    </div>
</body>
</html>
