<!DOCTYPE html>
<html>
<head>
    <title>Obtener Ciudad a Visitar</title>
    <style>
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        h1 {
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="number"] {
            padding: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        select {
            padding: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        button {
            background-color: #008CBA;
            color: #fff;
            padding: 10px 20px;
            border-radius: 5px;
            border: none;
            cursor: pointer;
        }

        button:hover {
            background
        }
        
    </style>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="container">
        <h1>Obtener Ciudad a Visitar</h1>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label for="sexo">Seleccione su sexo:</label>
            <select id="sexo" name="sexo" required>
                <option value="masculino">Masculino</option>
                <option value="femenino">Femenino</option>
            </select>
            <label for="puntaje">Ingrese su puntaje en el examen:</label>
            <input type="number" id="puntaje" name="puntaje" required>
            <button type="submit">Obtener Ciudad a Visitar</button>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $sexo = $_POST['sexo'];
            $puntaje = $_POST['puntaje'];

            // Obtener la ciudad a visitar
            $ciudad = "";

            if ($sexo == 'masculino') {
                if ($puntaje >= 18 && $puntaje <= 35) {
                    $ciudad = "Arequipa";
                } elseif ($puntaje >= 36 && $puntaje <= 75) {
                    $ciudad = "Cuzco";
                } elseif ($puntaje > 75) {
                    $ciudad = "Iquitos";
                }
            } elseif ($sexo == 'femenino') {
                if ($puntaje >= 18 && $puntaje <= 35) {
                    $ciudad = "Cuzco";
                } elseif ($puntaje >= 36 && $puntaje <= 75) {
                    $ciudad = "Iquitos";
                } elseif ($puntaje > 75) {
                    $ciudad = "Arequipa";
                }
            }

            echo "<h2>Resultado</h2>";
            echo "<p>Según el sexo $sexo y el puntaje obtenido en el examen ($puntaje), la ciudad a visitar es $ciudad.</p>";
        }
        ?>
    </div>
</body>
</html>
