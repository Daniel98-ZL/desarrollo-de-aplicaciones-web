<!DOCTYPE html>
<html>
<head>
    <title>Ordenar Números</title>
    <style>
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        h1 {
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="number"] {
            padding: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        button {
            background-color: #008CBA;
            color: #fff;
            padding: 10px 20px;
            border-radius: 5px;
            border: none;
            cursor: pointer;
        }

        button:hover {
            background-color: #006D9C;
        }

    </style>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="container">
        <h1>Ordenar Números</h1>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label for="numero1">Ingrese el primer número:</label>
            <input type="number" id="numero1" name="numero1" required>
            <label for="numero2">Ingrese el segundo número:</label>
            <input type="number" id="numero2" name="numero2" required>
            <label for="numero3">Ingrese el tercer número:</label>
            <input type="number" id="numero3" name="numero3" required>
            <button type="submit">Ordenar</button>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $numero1 = $_POST['numero1'];
            $numero2 = $_POST['numero2'];
            $numero3 = $_POST['numero3'];

            // Ordenar los números en forma ascendente
            $ascendente = [$numero1, $numero2, $numero3];
            sort($ascendente);

            // Ordenar los números en forma descendente
            $descendente = [$numero1, $numero2, $numero3];
            rsort($descendente);

            echo "<h2>Resultado</h2>";
            echo "<p>Números ordenados en forma ascendente: " . implode(", ", $ascendente) . "</p>";
            echo "<p>Números ordenados en forma descendente: " . implode(", ", $descendente) . "</p>";
        }
        ?>
    </div>
</body>
</html>
