<!DOCTYPE html>
<html>
<head>
    <title>Calcular Descuento de Sueldo</title>
    <style>
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        h1 {
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="number"] {
            padding: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        select {
            padding: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        button {
            background-color: #008CBA;
            color: #fff;
            padding: 10px 20px;
            border-radius:
        }
    </style>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="container">
        <h1>Calcular Descuento de Sueldo</h1>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label for="genero">Seleccione el género:</label>
            <select id="genero" name="genero" required>
                <option value="hombre">Hombre</option>
                <option value="mujer">Mujer</option>
            </select>
            <label for="cargo">Seleccione el cargo:</label>
            <select id="cargo" name="cargo" required>
                <option value="obrero">Obrero</option>
                <option value="empleado">Empleado</option>
            </select>
            <label for="sueldo">Ingrese el sueldo del trabajador:</label>
            <input type="number" id="sueldo" name="sueldo" required>
            <button type="submit">Calcular Descuento</button>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $genero = $_POST['genero'];
            $cargo = $_POST['cargo'];
            $sueldo = $_POST['sueldo'];

            // Calcular el monto de descuento
            $descuento = 0;

            if ($genero == 'hombre' && $cargo == 'obrero') {
                $descuento = $sueldo * 0.15;
            } elseif ($genero == 'hombre' && $cargo == 'empleado') {
                $descuento = $sueldo * 0.20;
            } elseif ($genero == 'mujer' && $cargo == 'obrero') {
                $descuento = $sueldo * 0.10;
            } elseif ($genero == 'mujer' && $cargo == 'empleado') {
                $descuento = $sueldo * 0.15;
            }

            echo "<h2>Resultado</h2>";
            echo "<p>El sueldo ingresado es $sueldo y el monto de descuento es $descuento.</p>";
        }
        ?>
    </div>
</body>
</html>
