<!DOCTYPE html>
<html>
<head>
    <title>Calculadora de Porcentajes</title>
    <style>
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        h1 {
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="number"] {
            padding: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        button {
            background-color: #008CBA;
            color: #fff;
            padding: 10px 20px;
            border-radius: 5px;
            border: none;
            cursor: pointer;
        }

        button:hover {
            background-color: #006D9C;
        }

    </style>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="container">
        <h1>Calculadora de Porcentajes</h1>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label for="num1">Número 1:</label>
            <input type="number" id="num1" name="num1" required>
            <label for="num2">Número 2:</label>
            <input type="number" id="num2" name="num2" required>
            <label for="num3">Número 3:</label>
            <input type="number" id="num3" name="num3" required>
            <label for="num4">Número 4:</label>
            <input type="number" id="num4" name="num4" required>
            <button type="submit">Calcular</button>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $num1 = $_POST['num1'];
            $num2 = $_POST['num2'];
            $num3 = $_POST['num3'];
            $num4 = $_POST['num4'];

            $suma = $num1 + $num2 + $num3 + $num4;

            $porcentaje1 = ($num1 / $suma) * 100;
            $porcentaje2 = ($num2 / $suma) * 100;
            $porcentaje3 = ($num3 / $suma) * 100;
            $porcentaje4 = ($num4 / $suma) * 100;

            echo "<h2>Resultado</h2>";
            echo "<p>Porcentaje del número 1: $porcentaje1%</p>";
            echo "<p>Porcentaje del número 2: $porcentaje2%</p>";
            echo "<p>Porcentaje del número 3: $porcentaje3%</p>";
            echo "<p>Porcentaje del número 4: $porcentaje4%</p>";
        }
        ?>
    </div>
</body>
</html>
