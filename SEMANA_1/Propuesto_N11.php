<!DOCTYPE html>
<html>
<head>
    <title>Determinar Mayoría de Edad</title>
    <style>
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        h1 {
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="number"] {
            padding: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        button {
            background-color: #008CBA;
            color: #fff;
            padding: 10px 20px;
            border-radius: 5px;
            border: none;
            cursor: pointer;
        }

        button:hover {
            background-color: #006D9C;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="container">
        <h1>Determinar Mayoría de Edad</h1>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label for="edad">Ingrese su edad:</label>
            <input type="number" id="edad" name="edad" required>
            <button type="submit">Determinar</button>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $edad = $_POST['edad'];

            $mensaje = ($edad >= 18) ? "Es mayor de edad" : "Es menor de edad";

            echo "<h2>Resultado</h2>";
            echo "<p>$mensaje</p>";
        }
        ?>
    </div>
</body>
</html>
