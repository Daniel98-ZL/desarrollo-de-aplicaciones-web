<!DOCTYPE html>
<html>
<head>
    <title>Calcular Suma y Producto</title>
    <style>
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        h1 {
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="number"] {
            padding: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        button {
            background-color: #008CBA;
            color: #fff;
            padding: 10px 20px;
            border-radius: 5px;
            border: none;
            cursor: pointer;
        }

        button:hover {
            background-color: #006D9C;
        }

    </style>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="container">
        <h1>Calcular Suma y Producto</h1>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label for="numero">Ingrese el valor de N:</label>
            <input type="number" id="numero" name="numero" required>
            <button type="submit">Calcular</button>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $numero = $_POST['numero'];

            // Calcular la suma y el producto de los N primeros múltiplos de 3
            $suma = 0;
            $producto = 1;
            for ($i = 1; $i <= $numero; $i++) {
                $multiplo = $i * 3;
                $suma += $multiplo;
                $producto *= $multiplo;
            }

            echo "<h2>Resultado</h2>";
            echo "<p>La suma de los $numero primeros múltiplos de 3 es $suma.</p>";
            echo "<p>El producto de los $numero primeros múltiplos de 3 es $producto.</p>";
        }
        ?>
    </div>
</body>
</html>
