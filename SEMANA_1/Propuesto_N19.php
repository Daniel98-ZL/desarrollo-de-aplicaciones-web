<!DOCTYPE html>
<html>
<head>
    <title>Determinar Triángulo</title>
    <style>
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        h1 {
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="number"] {
            padding: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        button {
            background-color: #008CBA;
            color: #fff;
            padding: 10px 20px;
            border-radius: 5px;
            border: none;
            cursor: pointer;
        }

        button:hover {
            background-color: #006D9C;
        }

    </style>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="container">
        <h1>Determinar Triángulo</h1>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label for="longitud1">Ingrese la longitud 1:</label>
            <input type="number" id="longitud1" name="longitud1" required>
            <label for="longitud2">Ingrese la longitud 2:</label>
            <input type="number" id="longitud2" name="longitud2" required>
            <label for="longitud3">Ingrese la longitud 3:</label>
            <input type="number" id="longitud3" name="longitud3" required>
            <button type="submit">Verificar</button>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $longitud1 = $_POST['longitud1'];
            $longitud2 = $_POST['longitud2'];
            $longitud3 = $_POST['longitud3'];

            // Verificar si forman un triángulo
            if (($longitud1 < $longitud2 + $longitud3) && ($longitud2 < $longitud1 + $longitud3) && ($longitud3 < $longitud1 + $longitud2)) {
                $mensaje = "Las longitudes ingresadas forman un triángulo.";
            } else {
                $mensaje = "Las longitudes ingresadas no forman un triángulo.";
            }

            echo "<h2>Resultado</h2>";
            echo "<p>$mensaje</p>";
        }
        ?>
    </div>
</body>
</html>
