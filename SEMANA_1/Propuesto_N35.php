<!DOCTYPE html>
<html>
<head>
    <title>Buscar Dígito</title>
    <style>
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        h1 {
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="number"] {
            padding: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        button {
            background-color: #008CBA;
            color: #fff;
            padding: 10px 20px;
            border-radius: 5px;
            border: none;
            cursor: pointer;
        }

        button:hover {
            background-color: #006D9C;
        }

    </style>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="container">
        <h1>Buscar Dígito</h1>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label for="numero">Ingrese un número:</label>
            <input type="number" id="numero" name="numero" required>
            <label for="digito">Ingrese el dígito a buscar:</label>
            <input type="number" id="digito" name="digito" required>
            <button type="submit">Buscar</button>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $numero = $_POST['numero'];
            $digito = $_POST['digito'];

            // Verificar si el número contiene el dígito
            $contieneDigito = false;
            $numStr = (string)$numero;
            for ($i = 0; $i < strlen($numStr); $i++) {
                if ($numStr[$i] == $digito) {
                    $contieneDigito = true;
                    break;
                }
            }

            echo "<h2>Resultado</h2>";
            if ($contieneDigito) {
                echo "<p>El número $numero contiene el dígito $digito.</p>";
            } else {
                echo "<p>El número $numero no contiene el dígito $digito.</p>";
            }
        }
        ?>
    </div>
</body>
</html>
