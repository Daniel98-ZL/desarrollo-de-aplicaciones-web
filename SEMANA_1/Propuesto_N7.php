<!DOCTYPE html>
<html>
<head>
    <title>Conversor de Horas a Minutos y Segundos</title>
    <style>
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        h1 {
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="number"] {
            padding: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        button {
            background-color: #008CBA;
            color: #fff;
            padding: 10px 20px;
            border-radius: 5px;
            border: none;
            cursor: pointer;
        }

        button:hover {
            background-color: #006D9C;
        }

    </style>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="container">
        <h1>Conversor de Horas a Minutos y Segundos</h1>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label for="horas">Ingrese la cantidad de horas:</label>
            <input type="number" id="horas" name="horas" required>
            <button type="submit">Convertir</button>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $horas = $_POST['horas'];

            $minutos = $horas * 60; // Obtener la cantidad de minutos
            $segundos = $horas * 3600; // Obtener la cantidad de segundos

            echo "<h2>Resultado</h2>";
            echo "<p>Cantidad ingresada: $horas horas</p>";
            echo "<p>Equivalente en minutos: $minutos minutos</p>";
            echo "<p>Equivalente en segundos: $segundos segundos</p>";
        }
        ?>
    </div>
</body>
</html>
