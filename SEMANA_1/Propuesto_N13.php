<!DOCTYPE html>
<html>
<head>
    <title>Determinar Igualdad de Números</title>
    <style>
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        h1 {
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="number"] {
            padding: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        button {
            background-color: #008CBA;
            color: #fff;
            padding: 10px 20px;
            border-radius: 5px;
            border: none;
            cursor: pointer;
        }

        button:hover {
            background-color: #006D9C;
        }

    </style>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="container">
        <h1>Determinar Igualdad de Números</h1>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label for="numero1">Ingrese el primer número:</label>
            <input type="number" id="numero1" name="numero1" required>
            <label for="numero2">Ingrese el segundo número:</label>
            <input type="number" id="numero2" name="numero2" required>
            <button type="submit">Determinar</button>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $numero1 = $_POST['numero1'];
            $numero2 = $_POST['numero2'];

            $mensaje = ($numero1 == $numero2) ? "Los números son iguales" : "Los números son diferentes";

            echo "<h2>Resultado</h2>";
            echo "<p>$mensaje</p>";
        }
        ?>
    </div>
</body>
</html>
