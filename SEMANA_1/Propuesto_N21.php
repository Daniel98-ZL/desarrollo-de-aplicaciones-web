<!DOCTYPE html>
<html>
<head>
    <title>Obtener Mes en Letras</title>
    <style>
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        h1 {
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="number"] {
            padding: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        button {
            background-color: #008CBA;
            color: #fff;
            padding: 10px 20px;
            border-radius: 5px;
            border: none;
            cursor: pointer;
        }

        button:hover {
            background-color: #006D9C;
        }

    </style>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="container">
        <h1>Obtener Mes en Letras</h1>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label for="numero_mes">Ingrese el número del mes:</label>
            <input type="number" id="numero_mes" name="numero_mes" required>
            <button type="submit">Obtener</button>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $numero_mes = $_POST['numero_mes'];

            // Obtener el nombre del mes en letras
            $meses = [
                1 => "Enero",
                2 => "Febrero",
                3 => "Marzo",
                4 => "Abril",
                5 => "Mayo",
                6 => "Junio",
                7 => "Julio",
                8 => "Agosto",
                9 => "Septiembre",
                10 => "Octubre",
                11 => "Noviembre",
                12 => "Diciembre"
            ];

            if ($numero_mes >= 1 && $numero_mes <= 12) {
                $nombre_mes = $meses[$numero_mes];
                echo "<h2>Resultado</h2>";
                echo "<p>El número $numero_mes corresponde al mes de $nombre_mes.</p>";
            } else {
                echo "<h2>Error</h2>";
                echo "<p>Ingrese un número de mes válido (entre 1 y 12).</p>";
            }
        }
        ?>
    </div>
</body>
</html>
