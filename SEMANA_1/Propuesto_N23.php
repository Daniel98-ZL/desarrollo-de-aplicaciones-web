<!DOCTYPE html>
<html>
<head>
    <title>Obtener Nombre del Operador</title>
    <style>
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        h1 {
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="text"] {
            padding: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        button {
            background-color: #008CBA;
            color: #fff;
            padding: 10px 20px;
            border-radius: 5px;
            border: none;
            cursor: pointer;
        }

        button:hover {
            background-color: #006D9C;
        }

    </style>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="container">
        <h1>Obtener Nombre del Operador</h1>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label for="operador">Ingrese el símbolo del operador:</label>
            <input type="text" id="operador" name="operador" required>
            <button type="submit">Obtener</button>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $operador = $_POST['operador'];

            // Obtener el nombre del operador
            $nombre_operador = "";

            switch ($operador) {
                case '+':
                    $nombre_operador = "Suma";
                    break;
                case '-':
                    $nombre_operador = "Resta";
                    break;
                case '*':
                    $nombre_operador = "Multiplicación";
                    break;
                case '/':
                    $nombre_operador = "División";
                    break;
                default:
                    echo "<h2>Error</h2>";
                    echo "<p>Ingrese un símbolo de operador válido (+, -, * o /).</p>";
                    break;
            }

            if (!empty($nombre_operador)) {
                echo "<h2>Resultado</h2>";
                echo "<p>El símbolo '$operador' corresponde al operador '$nombre_operador'.</p>";
            }
        }
        ?>
    </div>
</body>
</html>
