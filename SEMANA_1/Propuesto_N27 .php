<!DOCTYPE html>
<html>
<head>
    <title>Obtener Estado Civil</title>
    <style>
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        h1 {
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="number"] {
            padding: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        button {
            background-color: #008CBA;
            color: #fff;
            padding: 10px 20px;
            border-radius: 5px;
            border: none;
            cursor: pointer;
        }

        button:hover {
            background-color: #006D9C;
        }

    </style>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="container">
        <h1>Obtener Estado Civil</h1>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label for="codigo">Ingrese el código de estado civil:</label>
            <input type="number" id="codigo" name="codigo" required>
            <button type="submit">Obtener Estado Civil</button>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $codigo = $_POST['codigo'];

            // Obtener el nombre del estado civil
            $estadoCivil = "";

            switch ($codigo) {
                case 0:
                    $estadoCivil = "Soltero";
                    break;
                case 1:
                    $estadoCivil = "Casado";
                    break;
                case 2:
                    $estadoCivil = "Divorciado";
                    break;
                case 3:
                    $estadoCivil = "Viudo";
                    break;
                default:
                    $estadoCivil = "Desconocido";
                    break;
            }

            echo "<h2>Resultado</h2>";
            echo "<p>El código ingresado es $codigo y el estado civil correspondiente es $estadoCivil.</p>";
        }
        ?>
    </div>
</body>
</html>
