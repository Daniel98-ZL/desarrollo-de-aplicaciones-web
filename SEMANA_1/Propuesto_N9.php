<!DOCTYPE html>
<html>
<head>
    <title>Cálculo de Área y Perímetro de un Rectángulo</title>
    <style>
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        h1 {
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="number"] {
            padding: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        button {
            background-color: #008CBA;
            color: #fff;
            padding: 10px 20px;
            border-radius: 5px;
            border: none;
            cursor: pointer;
        }

        button:hover {
            background-color: #006D9C;
        }

    </style>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="container">
        <h1>Cálculo de Área y Perímetro de un Rectángulo</h1>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label for="base">Base:</label>
            <input type="number" id="base" name="base" required>
            <label for="altura">Altura:</label>
            <input type="number" id="altura" name="altura" required>
            <button type="submit">Calcular</button>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $base = $_POST['base'];
            $altura = $_POST['altura'];

            $area = $base * $altura;
            $perimetro = 2 * ($base + $altura);

            echo "<h2>Resultado</h2>";
            echo "<p>Área: $area</p>";
            echo "<p>Perímetro: $perimetro</p>";
        }
        ?>
    </div>
</body>
</html>
