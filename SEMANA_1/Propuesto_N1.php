
<?php
//Enunciado: Dado dos números enteros (Z), a y b, hallar a + b y a – b.
//Variables
$a = 0; $b = 0; $suma = 0; $resta = 0;
if(isset($_POST["btnCalcular"])) {
//Entrada
$a = (int)$_POST["Numero1"];
$b = (int)$_POST["Numero2"];
//Proceso
$suma = $a + $b;
$resta = $a - $b;
}
?>
<html>
<head>
<title>Propuesto N° 01</title>
<style type="text/css">
<!--
.TextoFondo {
background-color: rosybrown;
}
th, td {
   width: 25%;
   text-align: left;
   vertical-align: top;
   border: 1px solid #000;
}
td,strong {
    text-align: center;
    color: red;
}
-->
</style>
</head>
<body>
<form method="post" action="Propuesto_N1.php">
<table width="241" border="0">
<tr>
<td colspan="2"><strong>SUMA Y RESTA</strong> </td>
</tr>
<tr>
<td width="81">Numero 1 </td>
<td width="150">
<input name="Numero1" type="numero" id="Numero1" value="<?=$a?>" />
</td>
</tr>
<tr>
<td>Numero 2 </td>
<td>
<input name="Numero2" type="numero" id="Numero2" value="<?=$b?>"/>
</td>
</tr>
<tr>
<td>Suma</td>
<td>
<input name="txts" type="text" class="TextoFondo" id="txts"
value="<?=$suma?>"/>
</td>
</tr>
<tr>
<td>Resta</td>
<td>
<input name="txts" type="text" class="TextoFondo" id="txts"
value="<?=$resta?>"/>
</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>
<input name="btnCalcular" type="submit" id="btnCalcular"
value="Calcular" />
</td>
</tr>
</table>
</form>
</body>
</html>