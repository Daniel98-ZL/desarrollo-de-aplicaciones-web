<!DOCTYPE html>
<html>
<head>
    <title>Conversor de Milímetros a Metros</title>
    <style>
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        h1 {
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="number"] {
            padding: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        button {
            background-color: #008CBA;
            color: #fff;
            padding: 10px 20px;
            border-radius: 5px;
            border: none;
            cursor: pointer;
        }

        button:hover {
            background-color: #006D9C;
        }

    </style>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="container">
        <h1>Conversor de Milímetros a Metros</h1>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label for="milimetros">Ingrese la cantidad de milímetros:</label>
            <input type="number" id="milimetros" name="milimetros" required>
            <button type="submit">Convertir</button>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $milimetros = $_POST['milimetros'];

            $metros = floor($milimetros / 1000); // Obtener la cantidad de metros enteros
            $resto_metros = $milimetros % 1000; // Obtener el resto de milímetros después de calcular los metros
            $decimetros = floor($resto_metros / 100); // Obtener la cantidad de decímetros enteros
            $resto_decimetros = $resto_metros % 100; // Obtener el resto de milímetros después de calcular los decímetros
            $centimetros = floor($resto_decimetros / 10); // Obtener la cantidad de centímetros enteros
            $milimetros = $resto_decimetros % 10; // Obtener los milímetros restantes

            echo "<h2>Resultado</h2>";
            echo "<p>Cantidad ingresada: $milimetros milímetros</p>";
            echo "<p>$metros metros, $decimetros decímetros, $centimetros centímetros y $milimetros milímetros</p>";
        }
        ?>
    </div>
</body>
</html>
