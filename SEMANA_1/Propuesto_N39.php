<!DOCTYPE html>
<html>
<head>
    <title>Máximo Común Divisor</title>
    <style>
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        h1 {
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="number"] {
            padding: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        button {
            background-color: #008CBA;
            color: #fff;
            padding: 10px 20px;
            border-radius: 5px;
            border: none;
            cursor: pointer;
        }

        button:hover {
            background-color: #006D9C;
        }

    </style>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="container">
        <h1>Máximo Común Divisor</h1>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label for="numero1">Ingrese el primer número:</label>
            <input type="number" id="numero1" name="numero1" required>
            <label for="numero2">Ingrese el segundo número:</label>
            <input type="number" id="numero2" name="numero2" required>
            <button type="submit">Calcular</button>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $numero1 = $_POST['numero1'];
            $numero2 = $_POST['numero2'];

            // Función para calcular el MCD utilizando el algoritmo de Euclides
            function calcularMCD($a, $b) {
                while ($b != 0) {
                    $temp = $b;
                    $b = $a % $b;
                    $a = $temp;
                }
                return $a;
            }

            // Calcular el MCD de los dos números
            $mcd = calcularMCD($numero1, $numero2);

            echo "<h2>Resultado</h2>";
            echo "<p>El MCD de $numero1 y $numero2 es: $mcd</p>";
        }
        ?>
    </div>
</body>
</html>
