<!--Dados 4 números y almacénelo en un vector, luego obtenga la suma y el promedio de los
valores almacenados.-->
<!DOCTYPE html>
<html>
<head>
    <title>La suma y el promedio de los
valores almacenados</title>
    <style>
        
        .TextoFondo {
            background-color: #CCFFFF;
        }
        body{
            background-color: whitesmoke;
        }
        .TextoFondo {
        background-color: wheat;
        }
        th, td {
            width: 25%;
            text-align: left;
            vertical-align: top;
            border: 1px solid #000;
        }
        td,strong {
            text-align: center;
            color: black;
            background-color: grey;
        }
        .btnCalcular {
            background-color: wheat;
            cursor: pointer
        }
        .Numero1 {
            background-color: wheat;
        }
        .Numero2 {
            background-color: wheat;
        }
    </style>
</head>
<body>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        Ingrese n° 1: <input type="number" name="a"><br>
        Ingrese n° 2: <input type="number" name="b"><br>
        Ingrese n° 3: <input type="number" name="c"><br>
        Ingrese n° 4: <input type="number" name="d"><br>
        <input type="submit" value="Calcular">
    </form>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $a = $_POST["a"];
        $b = $_POST["b"];
        $c = $_POST["c"];
        $d = $_POST["d"];
        $suma = $a + $b + $c + $d; 
        $promedio= $suma / 4;
    }
    ?>
    <table>
    <tr>
        <td>La suma de numeros es:</td>
        <td>
        <input name="txts" type="text" class="TextoFondo" id="txts" value="<?=$suma?>" />
        </td>
    </tr>
    <tr>
        <td>El promedio de los numeros es:</td>
        <td>
        <input name="txts" type="text" class="TextoFondo" id="txts" value="<?=$promedio?>" />
        </td>
    </tr>
    </table>
</body>
</html>