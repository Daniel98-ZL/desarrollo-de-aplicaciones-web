<!DOCTYPE html>
<html>
<head>
    <title>Calculadora de descuentos y recargos</title>
    <style>
    .container {
    max-width: 600px;
    margin: 0 auto;
    padding: 20px;
    border: 1px solid #ccc;
    border-radius: 5px;
}

h1 {
    text-align: center;
}

label {
    display: block;
    margin-bottom: 10px;
}

input[type="number"],
select {
    padding: 5px;
    margin-bottom: 10px;
    border-radius: 5px;
    border: 1px solid #ccc;
}

button {
    background-color: #008CBA;
    color: #fff;
    padding: 10px 20px;
    border-radius: 5px;
    border: none;
    cursor: pointer;
}

button:hover {
    background-color: #006D
}
</style>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="container">
        <h1>Calculadora de descuentos y recargos</h1>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label for="monto">Monto de la compra:</label>
            <input type="number" id="monto" name="monto" required>

            <label for="tipo_cliente">Tipo de cliente:</label>
            <select id="tipo_cliente" name="tipo_cliente" required>
                <option value="G">Público en general</option>
                <option value="A">Cliente afiliado</option>
            </select>

            <label for="tipo_pago">Tipo de pago:</label>
            <select id="tipo_pago" name="tipo_pago" required>
                <option value="C">Al contado</option>
                <option value="P">En plazos</option>
            </select>

            <button type="submit">Calcular</button>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Función para obtener el % de recargo
            function Recargo($tipo_cliente) {
                if ($tipo_cliente == 'G') {
                    return 0.05;
                } elseif ($tipo_cliente == 'A') {
                    return 0.03;
                }
            }

            // Función para obtener el % de descuento
            function Descuento($tipo_pago) {
                if ($tipo_pago == 'C') {
                    return 0.10;
                } elseif ($tipo_pago == 'P') {
                    return 0.05;
                }
            }

            $monto = $_POST['monto'];
            $tipo_cliente = $_POST['tipo_cliente'];
            $tipo_pago = $_POST['tipo_pago'];

            $recargo = $monto * Recargo($tipo_cliente);
            $descuento = $monto * Descuento($tipo_pago);
            $total = $monto + $recargo - $descuento;

            echo "<h2>Resultado</h2>";
            echo "<p>Monto de la compra: $" . $monto . "</p>";
            echo "<p>Monto del recargo: $" . $recargo . "</p>";
            echo "<p>Monto del descuento: $" . $descuento . "</p>";
            echo "<p>Total a pagar: $" . $total . "</p>";
        }
        ?>
    </div>
</body>
</html>

