<!DOCTYPE html>
<html>
<head>
    <title>Determinar Tipo de Carácter</title>
    <style>
        .container {
            width: 300px;
            margin: 0 auto;
            padding: 20px;
        }

        h1 {
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input {
            width: 100%;
            padding: 5px;
            margin-bottom: 10px;
        }

        button {
            display: block;
            width: 100%;
            padding: 10px;
            background-color: #4CAF50;
            color: white;
            border: none;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Determinar Tipo de Carácter</h1>
        <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
            <label for="caracter">Ingrese un carácter:</label>
            <input type="text" name="caracter" id="caracter" required>
            <button type="submit">Determinar</button>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $caracter = $_POST["caracter"];

            // Verificar si es una vocal
            if (in_array(strtolower($caracter), ['a', 'e', 'i', 'o', 'u'])) {
                echo "<h2>Resultado</h2>";
                echo "<p>El carácter ingresado es una vocal</p>";
            }
            // Verificar si es una letra mayúscula
            elseif (ctype_upper($caracter)) {
                echo "<h2>Resultado</h2>";
                echo "<p>El carácter ingresado es una letra mayúscula</p>";
            }
            // Verificar si es una letra minúscula
            elseif (ctype_lower($caracter)) {
                echo "<h2>Resultado</h2>";
                echo "<p>El carácter ingresado es una letra minúscula</p>";
            }
            // Verificar si es un número
            elseif (ctype_digit($caracter)) {
                echo "<h2>Resultado</h2>";
                echo "<p>El carácter ingresado es un número</p>";
            }
            // Si no es ninguno de los anteriores, es un símbolo
            else {
                echo "<h2>Resultado</h2>";
                echo "<p>El carácter ingresado es un símbolo</p>";
            }
        }
        ?>
    </div>
</body>
</html>
