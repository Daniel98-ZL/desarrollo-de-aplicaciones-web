<!DOCTYPE html>
<html>
<head>
    <title>Verificador de Palíndromos</title>
    <style>
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        h1 {
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="text"] {
            padding: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        button {
            background-color: #008CBA;
            color: #fff;
            padding: 10px 20px;
            border-radius: 5px;
            border: none;
            cursor: pointer;
        }

        button:hover {
            background-color: #006D9C;
        }

    </style>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="container">
        <h1>Verificador de Palíndromos</h1>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label for="palabra">Ingrese una palabra:</label>
            <input type="text" id="palabra" name="palabra" required>
            <button type="submit">Verificar</button>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            function esPalindromo($palabra) {
                $palabra = strtolower($palabra); // Convertir la palabra a minúsculas
                $palabra = str_replace(' ', '', $palabra); // Eliminar los espacios en blanco
                $invertida = strrev($palabra); // Invertir la palabra

                if ($palabra === $invertida) {
                    return true;
                } else {
                    return false;
                }
            }

            $palabra = $_POST['palabra'];

            if (esPalindromo($palabra)) {
                echo "<p>La palabra '$palabra' es un palíndromo.</p>";
            } else {
                echo "<p>La palabra '$palabra' no es un palíndromo.</p>";
            }
        }
        ?>
    </div>
</body>
</html>
