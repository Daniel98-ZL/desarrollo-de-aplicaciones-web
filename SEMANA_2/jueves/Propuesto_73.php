<!DOCTYPE html>
<html>
<head>
    <title>Determinar Etapa de Vida</title>
    <style>
        .container {
            width: 300px;
            margin: 0 auto;
            padding: 20px;
        }

        h1 {
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input {
            width: 100%;
            padding: 5px;
            margin-bottom: 10px;
        }

        button {
            display: block;
            width: 100%;
            padding: 10px;
            background-color: #4CAF50;
            color: white;
            border: none;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Determinar Etapa de Vida</h1>
        <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
            <label for="edad">Edad:</label>
            <input type="number" name="edad" id="edad" required>
            <button type="submit">Determinar</button>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $edad = $_POST["edad"];

            if ($edad <= 12) {
                echo "<h2>Resultado</h2>";
                echo "<p>Eres un niño/a</p>";
            } elseif ($edad <= 19) {
                echo "<h2>Resultado</h2>";
                echo "<p>Eres un adolescente</p>";
            } elseif ($edad <= 59) {
                echo "<h2>Resultado</h2>";
                echo "<p>Eres un adulto/a</p>";
            } else {
                echo "<h2>Resultado</h2>";
                echo "<p>Eres un adulto/a mayor</p>";
            }
        }
        ?>
    </div>
</body>
</html>
