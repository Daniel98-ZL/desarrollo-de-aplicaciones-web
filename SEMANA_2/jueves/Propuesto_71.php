<!DOCTYPE html>
<html>
<head>
    <title>Calculadora de Área y Perímetro de Cuadrado</title>
    <style>
        .container {
            width: 300px;
            margin: 0 auto;
            padding: 20px;
        }

        h1 {
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input {
            width: 100%;
            padding: 5px;
            margin-bottom: 10px;
        }

        button {
            display: block;
            width: 100%;
            padding: 10px;
            background-color: #4CAF50;
            color: white;
            border: none;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Calculadora de Área y Perímetro de Cuadrado</h1>
        <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
            <label for="lado">Lado del cuadrado:</label>
            <input type="number" name="lado" id="lado" required>
            <button type="submit">Calcular</button>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $lado = $_POST["lado"];

            $area = pow($lado, 2);
            $perimetro = 4 * $lado;

            echo "<h2>Resultado</h2>";
            echo "<p>Área del cuadrado: $area</p>";
            echo "<p>Perímetro del cuadrado: $perimetro</p>";
        }
        ?>
    </div>
</body>
</html>