<?php
//Variables
$s_par = 0;
$s_impar = 0;
$n = 0;
$i;
if (isset($_POST["btnCalcular"])) {
    $n = (int)$_POST["txtn1"];
    for($i = 0 ; $i < $n ; $i++ ){
        if($i % 2 == 0) {
            $s_par = $s_par + pow($i,2);
        }else{
            $s_impar = $s_impar + pow($i,3);
        }
    }
}
?>

<html>

<head>
    <title>Suma de cuadrados y cubos </title>
    <style type="text/css">
        .TextoFondo {
            background-color: #CCFFFF;
        }
        body{
            background-color: whitesmoke;
        }
        .TextoFondo {
        background-color: wheat;
        }
        th, td {
            width: 25%;
            text-align: left;
            vertical-align: top;
            border: 1px solid #000;
        }
        td,strong {
            text-align: center;
            color: black;
            background-color: grey;
        }
        .btnCalcular {
            background-color: wheat;
            cursor: pointer
        }
        .Numero1 {
            background-color: wheat;
        }
        .Numero2 {
            background-color: wheat;
        }
    </style>
</head>

<body>
    <form method="post" action="propuesto_1.php">
        <table width="241" border="0">
</body>
<tr>
    <td colspan="2"><strong>Suma de cuadrados y cubos</strong> </td>
</tr>
<tr>
    <td width="81">Ingrese N: </td>
    <td width="150">
        <input name="txtn1" type="text" id="txtn1" value="<?= $n ?>" />
    </td>
</tr>
<tr>
    <td>Suma Pares:</td>
    <td>
        <input name="txts" type="text" class="TextoFondo" id="txts" value="<?= $s_par ?>" />
    </td>
</tr>

<tr>
    <td>Suma Impares:</td>
    <td>
        <input name="txts" type="text" class="TextoFondo" id="txts" value="<?= $s_impar ?>" />
    </td>
</tr>

<tr>
    <td>&nbsp;</td>
    <td>
        <input name="btnCalcular" type="submit" id="btnCalcular" value="Calcular" />
    </td>
</tr>
</table>
</form>
</body>
</html>
