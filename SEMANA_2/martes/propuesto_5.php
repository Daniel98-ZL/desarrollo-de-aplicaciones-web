<!DOCTYPE html>
<html>
<head>
    <title>Calculo de sumas y cantidades</title>
    <style>
        .TextoFondo {
            background-color: #CCFFFF;
        }
        body{
            background-color: whitesmoke;
        }
        .TextoFondo {
        background-color: wheat;
        }
        th, td {
            width: 25%;
            text-align: left;
            vertical-align: top;
            border: 1px solid #000;
        }
        td,strong {
            text-align: center;
            color: black;
            background-color: grey;
        }
        .btnCalcular {
            background-color: wheat;
            cursor: pointer
        }
        .Numero1 {
            background-color: wheat;
        }
        .Numero2 {
            background-color: wheat;
        }
    </style>
</head>
<body>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        Ingrese valor de a: <input type="number" name="a"><br>
        Ingrese valor de b: <input type="number" name="b"><br>
        <input type="submit" value="Calcular">
    </form>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $a = $_POST["a"];
        $b = $_POST["b"];

        $suma_pares = 0; 
        $cantidad_pares = 0; 
        $suma_impares = 0; 
        $cantidad_impares = 0; 
        $suma_multiplos_3 = 0; 
        $cantidad_multiplos_3 = 0; 

        for ($i = $a; $i <= $b; $i++) {
            if ($i % 2 == 0) { 
                $suma_pares += $i; 
                $cantidad_pares++; 
            } else { 
                $suma_impares += $i; 
                $cantidad_impares++; 
            }
            if ($i % 3 == 0) { 
                $suma_multiplos_3 += $i; 
                $cantidad_multiplos_3++; 
            }
        }
    }
    ?>
    <table>
    <tr>
        <td>La suma de numeros pares:</td>
        <td>
        <input name="txts" type="text" class="TextoFondo" id="txts" value="<?=$suma_pares ?>" />
        </td>
    </tr>
    <tr>
        <td>La cantidad de numeros pares</td>
        <td>
        <input name="txts" type="text" class="TextoFondo" id="txts" value="<?=$cantidad_pares?>" />
        </td>
    </tr>
    <tr>
        <td>La suma de numeros impares:</td>
        <td>
        <input name="txts" type="text" class="TextoFondo" id="txts" value="<?=$suma_impares ?>" />
        </td>
    </tr>
    <tr>
        <td>La cantidad de numeros impares</td>
        <td>
        <input name="txts" type="text" class="TextoFondo" id="txts" value="<?=$cantidad_impares ?>" />
        </td>
    </tr>
    <tr>
        <td>La suma de multiplos de 3</td>
        <td>
        <input name="txts" type="text" class="TextoFondo" id="txts" value="<?=$suma_multiplos_3 ?>" />
        </td>
    </tr>
    <tr>
        <td>La cantidad de multiplos de 3</td>
        <td>
        <input name="txts" type="text" class="TextoFondo" id="txts" value="<?=$cantidad_multiplos_3 ?>" />
        </td>
    </tr>
    </table>
</body>
</html>